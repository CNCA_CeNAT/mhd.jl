module mhd

function hyperbolic_tangent_transition_region(left, right, width)
  x = range(-1, 1, step=width)
  region = (right + left)/2.0 .+ (tanh.(x/width)) * (right - left)/2.0 
end

function main()
  print("  __  __ _    _ _____   _ _ 
 |  \\/  | |  | |  __ \\ (_) |
 | \\  / | |__| | |  | | _| |
 | |\\/| |  __  | |  | || | |
 | |  | | |  | | |__| || | |
 |_|  |_|_|  |_|_____(_) |_|
                      _/ |  
                     |__/   \n")
  cond_left = [1., 0., 0., 0., 1., .75, 1., 0.]
  cond_right = [.125, 0., 0., 0., 0.1, .75, -1., 0.]
  conds = hyperbolic_tangent_transition_region.(cond_left, cond_right, 0.0085)
end

main()

end # module mhd
