# mhd.jl

This project aims to implement the Implicit predictor-corrector central finite difference scheme for the equations of magnetohydrodynamic simulations described by Tsai et al. (2014) in the Julia programming language
